<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden sm:rounded-lg">
                <div class="p-6 m-4 bg-white border-b border-gray-200">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-warning align-self-center" href="{{ route('room.index') }}">Room Setting</a>
                        <i class="bi bi-hospital" style="font-size: 2rem;"></i>
                    </div>
                </div>
                <div class="p-6 m-4 bg-white border-b border-gray-200">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-primary align-self-center" href="{{ route('customer.index') }}">List Customer</a>
                        <i class="bi bi-person" style="font-size: 2rem;"></i>
                    </div>
                </div>
                <div class="p-6 m-4 bg-white border-b border-gray-200">
                    <div class="d-flex justify-content-between">
                        <a class="btn btn-success align-self-center" href="{{ route('booking.index') }}">Inventory Booking</a>
                        <i class="bi bi-card-list" style="font-size: 2rem;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>